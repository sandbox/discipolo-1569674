<?php
/**
 * @file
 * fserver.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function fserver_taxonomy_default_vocabularies() {
  return array(
    'server_type' => array(
      'name' => 'Server Type',
      'machine_name' => 'server_type',
      'description' => 'A features App server type (distribution!)',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
